package eu.mytthew;

public class Developer implements Employee {
	private String name;
	private int ID;

	public Developer(String name, int ID) {
		this.name = name;
		this.ID = ID;
	}

	@Override
	public void showEmployeeDetails() {
		System.out.println("ID: " + ID + " name: " + name);
	}
}
