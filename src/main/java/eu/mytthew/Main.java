package eu.mytthew;

public class Main {
	public static void main(String[] args) {
		Developer developer1 = new Developer("Mateusz", 25);
		Developer developer2 = new Developer("Maciej", 61);
		Company eleader = new Company();
		eleader.addEmployee(developer1);
		eleader.addEmployee(developer2);

		Manager manager1 = new Manager("Ireneusz", 13);
		Manager manager2 = new Manager("Bartlomiej", 20);
		Company sabre = new Company();
		sabre.addEmployee(manager1);
		sabre.addEmployee(manager2);

		Company microsoft = new Company();
		microsoft.addEmployee(eleader);
		microsoft.addEmployee(sabre);
		microsoft.showEmployeeDetails();
	}
}
