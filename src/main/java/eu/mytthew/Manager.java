package eu.mytthew;

public class Manager implements Employee {
	private String name;
	private int ID;

	public Manager(String name, int ID) {
		this.name = name;
		this.ID = ID;
	}

	@Override
	public void showEmployeeDetails() {
		System.out.println("ID: " + ID + " name: " + name);
	}
}
